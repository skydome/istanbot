package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"fmt"
	df "github.com/leboncoin/dialogflow-go-webhook"
)

type parameters struct {
	Name string `json:"name"`
}

func webhook(rw http.ResponseWriter, req *http.Request) {
	fmt.Println("serving to request : ", req.Body)
	var err error
	var i *df.Request

	decoder := json.NewDecoder(req.Body)
	if err = decoder.Decode(&i); err != nil {
		fmt.Println("Error occured: ", err)
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	defer req.Body.Close()

	// fmt.Println("request body is: ", req.Body)
	// decoder := json.NewDecoder(req.Body)
	// if err = decoder.Decode(&i); err != nil {
	// 	rw.WriteHeader(http.StatusBadRequest)
	// 	return
	// }

	fmt.Println("Query Result is: ", i)
	// Do things with the context you just retrieved

	fmt.Println("Parameters: ", i.QueryResult.Parameters)

	var params parameters

	if err = i.GetParams(&params); err != nil {
		fmt.Println("Error occured: ", err)
		rw.WriteHeader(http.StatusBadRequest)
		return
	}

	fmt.Println("parameters are: ", params)
	dff := &df.Fulfillment{
		FulfillmentMessages: df.Messages{
			df.ForGoogle(df.SingleSimpleResponse("welcome", "welcome")),
			{RichMessage: df.Text{Text: []string{"Welcome back  ", params.Name}}},
		},
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	json.NewEncoder(rw).Encode(dff)
}

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("$PORT must be set")
	}

	fmt.Println("starting istanbot with port: ", port)
	http.HandleFunc("/", webhook)
	fmt.Println(http.ListenAndServe(":"+port, nil))
}
